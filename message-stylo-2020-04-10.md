# Message du 10 avril 2020 à propos de Stylo et de la bibliographie

Objet : [Master HN] Stylo et la gestion de la bibliographie

Bonjour,

Nicolas et moi nous espérons que vous gérez au mieux la situation et surtout que vous allez bien.

Voici un premier message concernant Stylo et notamment la gestion de la bibliographie, un second message à propos de l'évaluation va suivre dans quelques minutes (les deux sont liés).

Suite à vos réalisations avec Stylo il est nécessaire de vous faire un rappel sur plusieurs points. Cela vous permettra de vous entraîner pour l'évaluation.

## Gestion de la bibliographie
La moitié d'entre vous ont réussi à insérer des citations et à générer automatiquement une bibliographie, certains et certaines d'entre vous doivent donc se référer aux différentes ressources ci-dessous pour parvenir à gérer une bibliographie dans Stylo :

- le tutoriel du cours : https://framagit.org/ur-masterhn/ecriturenumerique/-/blob/master/travauxdiriges/seance5_stylo.md ;
- la documentation de Stylo : https://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!pages/bibliographie.md ;
- et nous vous avons préparé une vidéo explicative, parce que parfois c'est tout simplement plus compréhensible de _voir_ les manipulations : https://archive.org/details/stylo-gestion-bibliographique

Vous devez prendre le temps de consulter ces ressources et de vérifier que vous parvenez à intégrer une bibliographie _dynamique_ dans Stylo. Nous allons revenir dans les prochaines heures par mail vers celles et ceux qui ne sont pas parvenus à intégrer des références. Les personnes qui n'ont pas indiqué leur document Stylo dans Codi : il n'est pas trop tard mais faites vite (donc pas dans 5 jours) ! Nous pouvons également prévoir des temps d'échange en visio si vous êtes bloqué·e (mais pas la veille du rendu de l'évaluation).

## Distinction fond/forme
Stylo vous permet de structurer un document, ne vous inquiétez pas de sa mise en forme, de son rendu graphique, ce qui compte c'est que le texte, la bibliographie et les métadonnées soient bien structurées.

## Métadonnées
Dans le volet latéral de droite vous pouvez renseigner beaucoup de métadonnées, n'oubliez pas de compléter les champs dont vous connaissez les informations, autrement votre document n'aura pas de titre (par exemple).
