# Devoir à la maison

## Analyser une édition numérique

L'objectif de ce devoir à la maison sera de porter votre analyse sur l'édition numérique d'un objet éditorial. Votre analyse devra reprendre les notions du cours et proposer une position critique sur les enjeux de l'édition numérique tels que nous les avons abordés pendant le cours. Vous devrez également vous appuyer sur les textes en lien avec le cours.

Le travail est individuel et doit être fait et rendu sur Stylo. Une documentation accompagnera le sujet.

Pour vous guider dans l'analyse, le devoir sera structuré en 5 questions. Chaque réponse pourra faire entre 8 et 10 lignes (sur la preview Stylo).

## Critères d'évaluation

- 50% : analyse critique
- 25% : utilisation des notions et des ressources en ligne (richesse, pertinence, références)
- 25% : édition sur Stylo (titre·s, métadonnées, notes de bas de page, hyperliens)

## Quand

Le sujet vous est remis vendredi 10 avril, votre travail est à rendre pour la semaine suivante **vendredi 17 avril minuit**.

Nous reviendrons sur vos travaux et l'analyse par écrit la semaine suivante (proposition de corrigé en prenant en compte vos réponses).

## Vous pouvez :

- faire des recherches en ligne
- utiliser des citations courtes
- utiliser des exemples en renvoyant vers des ressources en lignes
- nous posez des questions par mail ou en cliquant ici: [[nouvelle _issue_](https://framagit.org/ur-masterhn/ecriturenumerique/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)]

## Vous devez :

- insérer vos citations dans une phrase (1), ou dans un paragraphe dédié (2). Par exemple :
  1. `Philippe Aigrain se pose la question de «\ comment articuler des temps d'intensité d'interaction avec les autres\ » [@aigrain_alternance_2005].`
  2.

  ```markdown
  > Comment articuler des temps d'intensité d'interaction avec les autres, d'interaction parcellarisée avec les différents médias et des temps de recul, de mise à distance, de réflexion et de perception dans la continuité et le contexte\ ? Comment articuler l'expression individuelle et la production collective\ ? [@aigrain_alternance_2005]

  ```

- référencer vos sources ! utilisez les références bibliographiques, les notes de bas de page et des liens hypertextes dans le texte. Pour les références bibliographiques, vous devez utiliser les fonctionnalités de Stylo vous permettant de structurer une bibliographie (15 références maximum).
- faire un effort d'édition ! (sans oublier l'orthographe)


## Description du travail à réaliser

_Les humanités numériques_ est un ouvrage de Pierre Mounier publié par les Éditions de la Maison des sciences de l'homme en 2018.
Le livre est disponible sur plusieurs supports : en version papier en librairie ou bibliothèque, en versions numériques (HTML, EPUB, PDF) sur la plateforme [OpenEdition Books](https://books.openedition.org/), ainsi que sur d'autres plateformes.

Vous devez analyser la version web (HTML) du livre _Les humanités numériques_, accessible à cette adresse : https://books.openedition.org/editionsmsh/12006  
Le livre a été commenté sur Hypothesis, nous vous suggérons d'accéder aux annotations en consultant la version annotée : https://via.hypothes.is/https://books.openedition.org/editionsmsh/12006

Votre travail doit être structuré en reprenant les 5 questions suivantes :

1. Faites un résumé d'un chapitre au choix.
2. Faites une description de l'édition. Quel est son format de diffusion, quel est le support de lecture ? Que pouvez-vous dire de cette édition comparée aux autres éditions disponibles ?
3. Le format et le support de l'édition permettent une structuration et une navigation particulière. Décrivez ces éléments de structuration, de navigation. Aux regards de ces éléments et de l'appareil critique disponible, que pouvez-vous dire de l'énonciation éditoriale de cette version ?
4. Que pouvez vous dire de la plateforme sur laquelle l'ouvrage est publié ? D'un point de vue économique et juridique, comment l'ouvrage est-il rendu accessible ?
5. À partir de vos analyses précédentes et des notions du cours, quelle·s question·s ou quelle·s problématique·s vous semble·nt pertinente·s à poser ? Sans forcément y répondre, justifiez votre proposition de problématique·s.

Bonne lecture et bonne édition sur Stylo. Nous sommes à votre disposition pour toute question.
