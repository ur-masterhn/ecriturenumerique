## Qu'est ce qu'une annotation ?

Considérez les annotations comme les surlignages, les idées, les
commentaires, parfois les post-its, que vous inscrivez dans un livre ou
sur un article.
!["Annotation sauvage"](http://editorialisation.org/ediwiki/images/thumb/7/74/Annotation.jpg/300px-Annotation.jpg)

Sur le web, plusieurs outils existent pour annoter une page web ou un
document pdf en ligne. Dans ce cours, nous utilisons l'outil
[Hypothes.is](https://web.hypothes.is).
![Annotation Hypothesis](http://editorialisation.org/ediwiki/images/thumb/2/28/AnnotationHypothesis.png/600px-AnnotationHypothesis.png)

Une annotation Hypothes.is est composée ainsi :

-   un fragment surligné du texte à lire,
-   un commentaire libre,
-   des mots-clés.

Il est aussi possible de créer une "annotation de page" (_page note_), dans ce cas,
l'annotation est attachée au document entier, et non à un fragment de
texte. Ce peut être un moyen de faire un commentaire général sur le
texte.

## Objectif

L'annotation, au crayon ou en ligne, permet de procéder à une lecture
active et profonde du texte. C'est une aide à la compréhension, et dans
le même temps, c'est un support de réflexion pour appréhender les idées
de l'auteur et les problématiser **en fonction de ses propres
intérêts**. On ne lit pas un texte de la même manière selon l'objectif
que l'on poursuit. Ainsi, annoter un texte suppose d'adopter un
certain point de vue, en fonction de son objectif : lecture rapide,
collecte thématique, analyse critique, etc.

Dans votre cas, **l'objectif principal de l'annotation pourra être de
préparer une synthèse du texte** en rassemblant le maximum d'éléments\ :

-   notions clés
-   ébauche de définitions
-   problématiques
-   éléments de débat

Il ne s'agit pas d'une analyse du texte, mais bien de dégager un contenu
théorique à partir des lectures données.

## Procédure

L'annotation s'effectue au sein d'un groupe d'annotation dédié à votre classe. Vous devez
sélectionner votre groupe avant de commencer l'annotation (voir ci-dessous [la prise en main d'Hypothes.is](#prise-en-main-dhypothesis-prise-en-main-h)).

### Attentes

À titre de suggestions, voici le travail d'annotation qui pourrait être
attendu :

-   identifier les notions et mots-clés dans un texte,
-   identifier dans le texte : la problématique, la thèse/idées fortes,
    les enjeux,
-   poser une question sur un concept,
-   répondre à une question posée par un pair,
-   réagir au texte en adoptant une position critique (une question peut
    être critique), identifier une nouvelle problématique, etc.

### Collaborer

L'annotation en ligne n'est plus un travail solitaire. Les annotations
sont sociales et suscitent le dialogue et la collaboration. La
collaboration au sein de votre classe commence lorsque vous interagissez
ensemble, par exemple :

-   en posant une question ou en proposant une réponse à un pair,
-   en proposant une ébauche de définition d'une notion identifiée par
    un pair,
-   en questionnant la pertinence d'une annotation d'un pair,
-   en ajoutant des mots-clés ou des catégories à l'annotation d'un pair,
-   en débattant d'une idée du texte ou d'un pair,
-   etc.

### Utilisation des tags

Les tags doivent être ajoutés dans leur propre zone de saisie qui se
trouve sous le texte de l'annotation. Ils doivent être fermés en
utilisant le signe de ponctuation de la virgule. Leur utilisation permet
une meilleure gestion de la totalité des annotations écrites par les
membres de la classe. Nous pourrons différencier efficacement nos
annotations grâce à ces tags:

- `référence` : pour une référence (le titre de la référence et son auteur doivent parallèlement être ajoutés en commentaire),
- `définition` : pour identifier une définition,
- `problématique` : pour identifier une problématique ou un enjeu
- `question` : pour poser une question

## Prise en main d'Hypothes.is


Plusieurs tutoriels de démarrage sont proposés en ligne. En voici deux qui sont relativement similaires :

- Sur le carnet de blog Vertigo : [Tutoriel d’utilisation de l’outil d’annotation hypothes.is](https://vertigo.hypotheses.org/evaluations-et-commentaires-ouverts/tutoriel-dutilisation-de-loutil-dannotation-hypothes-is)
- Sur le site du cen-r (Centre d'expertise numérique pour la recherche) : [Hypothesis, un outil pour annoter le web](http://www.cen.umontreal.ca/espacedoc/hypothesis/)

1. Créer un compte : vous devez [vous créer un compte](https://hypothes.is/signup) sur Hypothes.is.
2. Lancer l'outil d'annotation : pour annoter une page web, vous pouvez utiliser le bookmarklet Hypothesis, ou coller l'url de la page à annoter ici : https://via.hypothes.is/.
3. Annoter : il suffit de surligner un passage du texte, un petit menu vous proposera soit de simplement surligner, soit d'annoter. Le surlignage est privé (pour votre lecture personnelle), tandis que l'annotation peut-être partagée avec le groupe d'annotation.

### Rejoindre le groupe d'annotation

Une fois que vous êtes identifié·e à Hypothes.is, vous pouvez rejoindre le groupe d'annotation de la classe en cliquant sur ce lien [hypothes.is/groups/2eMGXG6d/ur-masterhn-2020](https://hypothes.is/groups/2eMGXG6d/ur-masterhn-2020).

### Annoter au sein de son groupe

Hypothes.is vous permet d'annoter les pages web soit en public, soit en privé. Une troisième option est d'annoter en groupe. Dans ce cas, vos annotations ne seront visibles qu'au sein du groupe. Pour cela, il vous faudra sélectionner le groupe dans le volet d'annotation, avant de démarrer l'annotation.
![Sélectionner son groupe d'annotation](http://editorialisation.org/ediwiki/images/thumb/1/15/Groupannotation.png/600px-Groupannotation.png)
