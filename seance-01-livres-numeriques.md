# Séance 01 – Livres numériques à choisir
Voici plusieurs livres numériques, merci d'en choisir un, nous nous en servirons pour le tour de table de la première séance :

## Resilient Web Design, Jeremy Keith
[![](images/rwd.png)](https://resilientwebdesign.com/)

## La social-démocratie est une chanson à boire, Otto Borg
[![](images/la-social-democratie-est-une-chanson-a-boire.jpg)](https://txt.abrupt.ch/antilivre/abrupt_borg_otto_la_social-democratie_est_une_chanson_a_boire_antilivre_dynamique.html)

## Professional Web Typography, Donny Truong
[![](images/prowebtype.png)](https://prowebtype.com)

## The Economy, The CORE team
[![](images/economy.png)](https://core-econ.org/the-economy/)

## Seeing Theory, Daniel Kunin, Jingru Guo, Tyler Dae Devlin, Daniel Xiang
[![](images/seeing.png)](https://seeing-theory.brown.edu/)

## Ancient Terracottas, Maria Lucia Ferruzza
[![](images/terracottas.png)](https://www.getty.edu/publications/terracottas/)

## Les sculptures de la villa romaine de Chiragan, Pascal Capus
[![](images/villachiragan.png)](https://villachiragan.saintraymond.toulouse.fr/)

## Exigeons de meilleures bibliothèques, David R. Lankes
[![](images/exigeons.png)](https://ateliers.sens-public.org/exigeons-de-meilleures-bibliotheques/)

## Sur l'image
[![](images/surlimage.png)](http://surlimage.info/)

## S.I.Lex, le blog revisité, Mélanie Leroy-Terquem et Sarah Clément
[![](images/silex.jpg)](https://books.openedition.org/pressesenssib/8736)
