# Écritures numériques et éditorialisation

## Internet et web

séance 02/10 - 13 février 2020

---
<small>Master Humanités numériques de l'Université de Rouen</small>

===d===

## Plan de la séance

1. Une brève histoire d'Internet
2. Une brève histoire du web
3. Accès et production d'information sur le web

===d===

## 1. Une brève histoire d'Internet

1. une histoire des communications
2. des paquets neutres, des adresses et des protocoles
3. Internet et le web

===d===

https://www.arte.tv/fr/videos/050970-001-A/world-brain/

===d===

## 1.1. Une histoire des communications
Quelques dates parmi d'autres :

- 1958 : premier câble transatlantique entre les États-Unis et l'Europe<!-- .element: class="fragment" -->
- 1876 : invention du téléphone<!-- .element: class="fragment" -->
- 1952 : premier ordinateur<!-- .element: class="fragment" -->
- 1969 : ARPAnet est le premier réseau à transfert de paquets développé aux États-Unis par la DARPA<!-- .element: class="fragment" -->
- 1974 : TCP/IP<!-- .element: class="fragment" -->

===n===


===d===

<!-- .slide: data-background="images/submarine-cable-map.png" -->

===n===
Voir le site https://www.submarinecablemap.com.

===d===

## 1.1. Une histoire des communications
Pour en savoir plus sur l'histoire des télécommunications : le livre _6|5_ d'Alexandre Laumonier aux éditions Zones sensibles.

===n===
Où l'on comprend que le développement des télécommunications est lié au développement économique et à la diffusion du savoir.

===d===

## 1.2. Des paquets neutres, des adresses et des protocoles

- Internet : un réseau de réseaux<!-- .element: class="fragment" -->
- décentraliser le réseau et sa gouvernance<!-- .element: class="fragment" -->
- tous les paquets sont libres et égaux !<!-- .element: class="fragment" -->
- chaque machine est identifiée avec son adresse IP<!-- .element: class="fragment" -->
- une série de protocoles : TCP (Transmission Control Protocol) et IP (Internet Protocol)<!-- .element: class="fragment" -->

===n===
L'aiguillage de paquet permet la décentralisation afin d'éviter que des dégradations liées à des combats puissent remettre en cause l'intégrité du réseau.
Il faut noter que l’accès à l’Internet n’était pas permis aux individus et utilisateurs commerciaux jusqu’au début des années 1990.

Personne ne possède ni ne contrôle l’Internet, même si certaines organisations ont la responsabilité de fixer les normes techniques, comme l'IETF (Internet Engineering Task Force) ou le W3C (World Wide Web Consortium).

Le concept de Neutralité du Net apparaît en 2003 dans un article du professeur de droit Tim Wu. Benjamin Bayart, pionnier de l'internet en France, a proposé quatre principes essentiels à la neutralité du Net :

* transmission des données par les opérateurs sans en examiner le contenu ;
* transmission des données sans prise en compte de la source ou de la destination des données (voir conflit Free/Youtube) ;
* transmission des données sans privilégier un protocole de communication (voir limitation pour la voix sur IP) ;
* transmission des données sans en altérer le contenu.

===d===

## 1.2. Des paquets neutres, des adresses et des protocoles

- le fractionnement des messages en paquets<!-- .element: class="fragment" -->
- l'utilisation d'un système d'adresses<!-- .element: class="fragment" -->
- l'acheminement des données sur le réseau (routage)<!-- .element: class="fragment" -->
- le contrôle des erreurs de transmission de données<!-- .element: class="fragment" -->

===n===
Ces critères s'expliquent notamment parce que TCP/IP a d'abord été créé dans un but militaire, il fallait ainsi un système fiable, résilient et distribué.
Il faut noter que ce protocole permet le transfert le plus rapide, et non le chemin le plus court, ce qui peut parfois expliquer des parcours surprenant (une connexion établie entre deux ordinateurs à Rouen passera peut-être par Montréal).

Le DNS (ou Domain Name System) permet de traduire un nom de domaine en adresse IP.
Le DNS rend Internet plus lisible, plus digeste et plus funky (http://www.fanzinecamping.cool).

Depuis une dizaine d'années les noms de domaine se diversifient, notamment grâce à des _domaines de premier niveau_, ou extensions, plus originaux : .cool, .plus, .xyz, .fish, .tatoo, etc. Pour avoir une idée de cette diversité, rendez-vous sur le site d'un _registrar_ qui délivre des noms de domaine, comme OVH ([https://www.ovh.com/fr/domaines/tarifs/](https://www.ovh.com/fr/domaines/tarifs/)) par exemple.

===d===

## 1.3. Internet et le web

# Internet ≠ Web

===d===

## 1.3. Internet et le web

# Internet[web]

===n===
Si vous ne deviez retenir qu'une seule chose c'est cela : Internet et le web ne sont pas la même chose, le web est un service d'Internet.

Même l'expression est entrée dans le langage courant, "je vais sur internet" ne veut pas dire grand chose lorsqu'on utilise un navigateur.

===d===

## 2. Une brève histoire du web

1. origine : partager des documents
2. un protocole et un langage
3. un accès universel qui se transforme en minitel

===d===

## 2.1. Origine : partager des documents

- invention de Tim Berners-Lee en 1989 au CERN<!-- .element: class="fragment" -->
- développement avec Robert Cailliau<!-- .element: class="fragment" -->
- objectif : partager des documents (via l'hypertexte)<!-- .element: class="fragment" -->

===n===
Le Web a été inventé au CERN par Tim Berners-Lee en 1989, rejoint par Robert Cailliau en 1990, pour partager des documents de recherche et palier aux problèmes de compatibilité des formats de document.

===d===

## 2.2. Un protocole et un langage
Comme Internet, le web est basé sur des protocoles normalisés et standards :

- HTTP comme protocole de communication<!-- .element: class="fragment" -->
- HTML comme langage de structuration<!-- .element: class="fragment" -->

===n===
HTTP permet de communiquer entre un serveur et un client, et HTML permet d'afficher des documents structurés.

===d===

## 2.2. Un protocole et un langage
Le fonctionnement du web en **4** principes :

* HTTP : un protocole de communication utilisant Internet<!-- .element: class="fragment" -->
* l'hypertexte : les pages web sont reliées entre elles grâce à des liens hypertextes<!-- .element: class="fragment" -->
* HTML pour décrire les pages web<!-- .element: class="fragment" -->
* un serveur HTTP sert des ressources à un client HTTP (un navigateur)<!-- .element: class="fragment" -->

===d===

## 2.3. Un accès universel qui se transforme en minitel
Une page web est visible partout de la même façon.

L'accès et la lecture ne sont pas limités par des problèmes :<!-- .element: class="fragment" -->

- de compatibilité<!-- .element: class="fragment" -->
- de formats<!-- .element: class="fragment" -->
- de logiciels<!-- .element: class="fragment" -->
- de systèmes d'exploitation<!-- .element: class="fragment" -->

(Vision positive 🌈)<!-- .element: class="fragment" -->

===d===

## 2.3. Un accès universel qui se transforme en minitel
Le web est désormais majoritairement limité à quelques grandes plateformes, la plupart des internautes utilisant le même moteur de recherche et consommant des contenus dans des jardins fermés.

(Vision négative 💩)<!-- .element: class="fragment" -->

===n===
Pour celles et ceux qui ne connaîtraient pas le Minitel : il s'agit d'un terminal permettant d'accéder à des contenus centralisés.

===d===

## 3. Accès et production d'information sur le web

1. modes d'accès aux pages web (moteurs de recherche)
2. des algorithmes irrespectueux
3. des outils pour produire des documents numériques

===d===

## 3.1. Modes d'accès aux pages web
Des annuaires aux moteurs de recherche en passant par les silos.

(Silos : espaces numériques cloisonnés.)<!-- .element: class="fragment" -->

===n===
Ce qu'il faut retenir ici c'est que dans tous les domaines et pour tous les usages, le recours à un moteur de recherche pour accéder à de l'information (et pas forcément pour _rechercher_ de l'information) est majoritaire.

===d===

## 3.2. Des algorithmes irrespectueux
L'accès à l'information via des plateformes se fait grâce à des algorithmes, qui favorisent certaines informations.

(Les algorithmes sont des recettes complexes conçues par des humains pour des machines.)<!-- .element: class="fragment" -->

===n===
Il n'est pas possible de créer des algorithmes neutres.

===d===

## 3.3. Des outils pour produire des documents numériques
Le web connaît deux périodes :

- avant 2000 : nécessité de passer par du code<!-- .element: class="fragment" -->
- après 2000 : interfaces faciles à utiliser<!-- .element: class="fragment" -->

===n===
Ce qui signifie qu'avant 2000 la création de contenus sur le web est réservée à des personnes ayant de bonnes connaissances.

À partir de 2000 il est possible de créer facilement un site web.
Le CMS le plus connu est sans doute WordPress, qui est un peu au web ce que la photocopieuse est au livre.

(Aujourd'hui créer un site semble superflu, une présence sur quelques plateformes suffit pour certains besoins.)

===d===

## 3.3. des outils pour produire des documents numériques
WYSIWYG partout : confusion entre le rendu à l'écran et la structuration des informations.

(What You See Is What You Get)<!-- .element: class="fragment" -->
