# Écritures numériques et éditorialisation

## L'édition scientifique ?

séance 05/10 - 13 mars 2020

---
<small>Master Humanités numériques de l'Université de Rouen</small>

===d===

## Plan de la séance

1. L'écosystème académique
2. Les particularités de l'édition scientifique
3. Le numérique et la recherche

===d===

## 1. L'écosystème académique

- des publics divers
- une temporalité longue
- la bibliométrie

===d===

### 1.1. Des publics divers
Plusieurs types de publics sont visés par les publications scientifiques :

- les chercheurs<!-- .element: class="fragment" -->
- les étudiants<!-- .element: class="fragment" -->
- la société civile<!-- .element: class="fragment" -->

===n===
Autant de publics avec des attentes et des niveaux de lecture différents.

===d===

### 1.2. Une temporalité longue
La recherche se fait dans une temporalité longue :

- les livres que nous lisons ont été longs à écrire et à éditer<!-- .element: class="fragment" -->
- système de révision et de validation nécessaire<!-- .element: class="fragment" -->
- en moyenne : minimum 6 mois pour un article et 1 an pour un livre<!-- .element: class="fragment" -->
- contraintes économiques : économie du savoir et moins économie financière<!-- .element: class="fragment" -->

===n===
Notons qu'il y a également différents paliers de publication de la recherche :

1. un travail en laboratoire
2. une communication pour un colloque
3. une proposition d'article
4. un chapitre d'un ouvrage
5. un livre
6. une travail en laboratoire
7. etc.

===d===

### 1.3. La bibliométrie
La publication scientifique repose en grande partie sur la bibliométrie :

- "analyse quantitative de l'activité et des réseaux scientifiques"<!-- .element: class="fragment" -->
- mesure du nombre de publications<!-- .element: class="fragment" -->
- mesure des liens entre publications<!-- .element: class="fragment" -->

===n===
La bibliométrie a un fonctionnement proche du _page rank_ de Google : mesure des liens entre les publications. Exemples :

- un

===d===

## 2. Les particularités de l'édition scientifique

- Communication scientifique
- Fonctions de l'éditeur scientifique
- Pratiques éditoriales

===d===

### 2.1 Communication scientifique

L'édition scientifique est une forme de communication scientifique. Pour le chercheur, les objectifs sont&nbsp;:

- Enregistrer ses recherches : construire une reconnaissance<!-- .element: class="fragment" -->
- Faire valider ses recherches :<!-- .element: class="fragment" -->
- Partager ses recherches : participer à la connaissance<!-- .element: class="fragment" -->

<!-- .element style="font-size:0.8em" -->


===d===

### 2.2 Pratiques éditoriales

L'édition scientifique impose des exigences particulières :

- Appareil critique (notes, références bibliographiques, index, etc.)<!-- .element: class="fragment" -->
- Citabilité du document &rarr; métadonnées (titre, résumé, mots-clés)<!-- .element: class="fragment" -->
- Identification des sources & références citées<!-- .element: class="fragment" -->
- Structuration rigoureuse (balisage non-ambigu, balisage sémantique)<!-- .element: class="fragment" -->
- Pérennité du document et des données<!-- .element: class="fragment" -->

<!-- .element style="font-size:0.8em" -->

===n===

→ utiliser des formats qui répondent à ces exigences, permettant notamment d'expliciter la fonction de chaque élément du document (titre, citation, métadonnées, références bibliographiques, etc.)

===d===

### 2.3 Fonctions de l'éditeur scientifique

- Structuration : proposer un format et un standard<!-- .element: class="fragment" -->
- Validation de la scientificité :<!-- .element: class="fragment" -->
  - vérification de l'appareil critique, notamment les références<!-- .element: class="fragment" -->
  - originalité (plagiat)<!-- .element: class="fragment" -->
  - évaluation par les pairs<!-- .element: class="fragment" -->
- Pérennisation : rassembler les conditions de la pérennité (standard, diffusion, interopérabilité, etc.)<!-- .element: class="fragment" -->
- Diffusion<!-- .element: class="fragment" -->

<!-- .element style="font-size:0.8em" -->


&rarr; Production d'autorité


===d===

## 3. L'édition scientifique numérique

- la place du numérique dans la recherche
- une timidité face au numérique
- quelques exemples de l'opportunité du numérique
- nouveaux acteurs : désintermédiation ?

===d===

### 3.1. La place du numérique dans la recherche
Le numérique est partout, c'est également dans la recherche scientifique&nbsp;:

- chercher et accéder à l'information<!-- .element: class="fragment" -->
- communiquer entre chercheurs<!-- .element: class="fragment" -->
- éditer la recherche<!-- .element: class="fragment" -->
- publier la recherche<!-- .element: class="fragment" -->

===n===
Sans les bases de données, dépôts et autres bibliothèques numériques, la recherche n'aurait pas le visage qu'elle a aujourd'hui.

===d===

### 3.2. Une timidité face au numérique
Actuellement les revues et les éditeurs utilisent encore trop peu "le numérique"&nbsp;:

- modèle numérique en grande partie calqué sur celui de l'imprimé<!-- .element: class="fragment" -->
- publications pauvres en terme de fonctionnalités ou de liens<!-- .element: class="fragment" -->
- apprentissage numérique nécessaire (culture, connaissances, pratiques, partage)<!-- .element: class="fragment" -->

===n===
Le numérique ce n'est pas que la "numérisation" des publications, c'est-à-dire des versions dématérialisées, mais aussi et surtout des publications enrichies (métadonnées, sémantique, médias, interactivité), c'est-à-dire quelque chose que ne peut pas proposer le numérique (ou pas facilement).

Le constat est qu'actuellement la réelle prise en compte du numérique est encore rare.

===d===

### 3.3. Quelques exemples numériques

- diffusion : Érudit/Cairn.info/HAL<!-- .element: class="fragment" -->
- production : [la revue Distill](https://distill.pub/)

<!-- .element: class="fragment" -->

- édition : [la revue PLOS](https://journals.plos.org/plosone/article/comment?id=10.1371/annotation/4e63c3af-76af-4c5f-bd86-2e5353ea48ad)

<!-- .element: class="fragment" -->
