# Écritures numériques et éditorialisation

## Qu'est-ce que l'édition numérique ?

séance 04/10 - 5 mars 2020

---
<small>Master Humanités numériques de l'Université de Rouen</small>

===d===

## Plan de la séance

1. Édition
2. Numérique
3. Édition numérique et éditorialisation
4. Livre numérique

===d===

## 1. Édition

===d===

### qu'est-ce que l'édition ?

_Comment vous le définissez ?_

===n===

Question posée à la classe.

traditionnellement, l'édition c'est :

- une étape intermédiaire entre l'écriture d'un contenu et sa publication.
- un procédé pour transformer un contenu brut en objets que l'on peut diffuser
- un métier, avec ses codes, ses institutions, ses formations
- une fonction : fonction sociale, économique, politique

L'édition consiste en de multiples tâches, selon les projets, les formes d'écritures et/ou les médiums visés (un livre, un rapport, un article, un film)

===d===

## 1.1 Fonctions éditoriales

- sélection
- production
- légitimation
- circulation

===n===

En tant qu'intermédiaire entre un auteur et un lectorat, l'édition remplit diverses fonctions :

- Sélection : prendre des décisions
- Production : améliorer le texte, le structurer et le mettre en forme, réaliser l'objet final: participe à la production de sens : «énonciation éditoriale» (Souchier)
- Légitimation : en tant qu'instance, l'éditeur ou la maison d'édition (pour un film le producteur), donne une certaine garantie sur le contenu qu'il a sélectionné
- Circulation :

la légitimité est qlq chose de complexe, elle peut venir de la renommée d'un éditeur, mais comment cette renommée s'est-elle construite ? Comment l'éditeur peut garantir l'intérêt ou l'importance d'un contenu ?

===d===

## 1.2 Énonciation éditoriale

> «&nbsp;Je peux définir l’énonciation éditoriale comme un "texte second" dont le signifiant n’est pas constitué par les mots de la langue, mais par **la matérialité du support et de l’écriture**, l’organisation du texte, sa mise en forme, bref par **tout ce qui en fait l’existence matérielle**.\
> [...]\
> L’une des fonctions premières de l’énonciation éditoriale est de **donner le texte à lire** comme activité de lecture (c’est sa dimension fonctionnelle, pragmatique ; on parlera alors de lisibilité). Dans un deuxième temps, elle s’inscrit dans l’histoire des formes du texte et par là même **implique un certain type de légitimité** ou d’illégitimité. L’énoncé de cette "énonciation" n’est donc pas le texte (le discours de l’auteur), mais la forme du texte, son image.&nbsp;»
>
> &mdash; Souchier, Emmanuël. « L'image du texte pour une théorie de l'énonciation éditoriale », _Les cahiers de médiologie_, vol. 6, no. 2, 1998, pp. 137-145. [(voir la citation)](https://hyp.is/hX-ZWACuEeqlhRf5gry1Hg/www.cairn.info/revue-les-cahiers-de-mediologie-1998-2-page-137.htm)

<!-- .element style="font-size:0.5em" -->

- Production de sens

===n===

Lire la citation

on retrouve la question de la production > fabriquer le support de lecture et la forme du texte + la légitimité.

Ce qu'il faut retenir: **La forme d'un contenu participe aussi à la production du sens**

Ex: Publier dans Libération ce n'est pas la même chose que publier sur un Blog. Publier dans Libération, ce n'est pas la même chose que publier dans le Figaro. Une édition Poche véhicule d'autres valeurs qu'une édition originale, ou qu'une édition critique dans la Pléiade. Et pour faire un teaser sur la suite, la notice _Féminisme_ de l'encyclopédie Universalis ne sera pas la même que la notice _Féminisme_ de Wikipédia.

===d===

## 1.2 Protocole éditorial

===d===

## 2. Numérique


===d===

## 2.1 Numérique (adj.)

On/Off | Binaire | Encodage
:-:|:-:|:-:
![on/off](https://upload.wikimedia.org/wikipedia/commons/4/47/Imagen_4.png) <!--.element style="height:8em;"--> | ![binary](https://cdn.pixabay.com/photo/2015/11/28/10/52/binary-1066983_960_720.jpg) <!--.element style="height:8em;"--> | ![alphabet](https://tse2.mm.bing.net/th?id=OIP.BDjU9sbrbDRSKMCArIuQ5ADbEc&pid=15.1&P=0&w=300&h=300) <!--.element style="height:8em;"--> |

===n===

- **Qu'est ce que le numérique ?** technologies basées sur des micros signaux électriques On/Off, que l'on interprète comme des 0 et 1.
- Ces 0/1 peuvent être combinés pour exprimer des symboles, typiquement un alphabet.
- cet alphabet nous permet d'écrire en langage naturel ou en langage informatique, des suites de symboles que l'ordinateur (ou en fait toute technologie numérique) saura interpréter en 0/1, et ainsi saura manipuler (calculer), stocker (dans des disques, ou des cartes mémoires), et bien-sûr afficher en retour sur un écran pour les rendre lisibles à l'humain.

Donc l'enjeu ici, vous le comprenez, c'est que les inscriptions deviennent manipulables, parfois calculable, réplicable à l'identique. Et cela change évidemment tout.

===d===

## 2.2 Analogique vs numérique

![Analog/digital](http://vitalirosati.net/slides/img/fra3825/num-an.jpg) <!--.element style="width:15em;"-->

- du continu au discret
- discrétisation &rarr; modélisation

===n===
===d===

## 2.3 _Le_ numérique ?

- une technologie de calcul : processeur
- une technologie de réseau : Internet
- une technologie de publication : le Web

&rarr; un fait culturel

===n===

Mais quand on parle aujourd'hui du numérique, "le numérique", "l'ère numérique" : c'est pour désigner  ses effets. Quelque chose de bcp plus vaste que ce qui se passe dans l'ordinateur, dans les smartphones, les frigidaires ou les cartes de crédits.

On peut présenter "le numérique" comme la conjonction de trois éléments:

- une technologie de calcul : c'est le processor, ce qui équipe tous les objets numériques ou informatique. C'est lui qui vient en premier
- une technologie de réseau : c'est internet, et la mise en réseau des machines et des objets
- une technologie de publication : c'est le web, en tant qu'espace d'écriture et de lecture

Ce n'est pas tout. Le numérique n'est pas qu'une conjonction de technologies. Avec Internet et le web, le numérique n'est plus un simple fait technologique, c'est **un fait culturel**.

===d===

## 2.4 Fait culturel ?

<div>
<div style="float:left;width:45%">

![Printing press](https://upload.wikimedia.org/wikipedia/commons/c/c6/Metal_movable_type_edit.jpg)

Printing press movable type  
[commons.wikimedia.org](https://commons.wikimedia.org/wiki/File:Metal_movable_type_edit.jpg)
<!-- .element style="font-size:0.7em" -->

</div>

<div style="float:left;width:45%">

![Printing press](https://upload.wikimedia.org/wikipedia/commons/4/40/La_masacre_de_San_Bartolom%C3%A9%2C_por_Fran%C3%A7ois_Dubois.jpg)

_Le Massacre de la Saint-Barthélemy_, François Dubois   
[commons.wikimedia.org](https://commons.wikimedia.org/wiki/File:La_masacre_de_San_Bartolom%C3%A9,_por_Fran%C3%A7ois_Dubois.jpg?uselang=fr)
<!-- .element style="font-size:0.7em" -->

</div></div>

===n===

Révolution similaire dans l'histoire ? On fait souvent référence à l'émergence de l'imprimerie dans l'Europe du 15eme et 16eme siècle comme élément de comparaison.

Ces bouleversements à l'époque ont été longs, entrainant des soubresauts sur plusieurs siècles. Ces transformations sociales et culturelles continuent de structurer notre monde actuel.

Un exemple: la Réforme protestante qui découle directement de la capacité à produire des ouvrages imprimés en dehors de l'autorité de l'institution catholique. Cad la capacité de publier des textes et des idées nouvelles (liberté intellectuelle), et le tout en plus grande quantité (plus large diffusion) tant les coûts de production étaient négligeables comparés au labeur d'un copiste.


===d===

## 2.5 Théorie du support

- de la « raison orale » à la « raison graphique » (Goody, 1979)
- ... à la « raison computationnelle » (Bachimont, 1996)

Le support de lecture et d'écriture conditionne la pensée et la cognition

===n===

Goody montre en fait que le passage d'une société orale à une société écrite a été une rupture majeure sur le plan de la cognition, cad que l'humanité ne pense pas de la même façon à partir du moment où elle se met à écrire.

Passage de la raison orale à la raison écrite, ou plutôt «graphique».

On pense avec et par le support de la pensée, cad le support d'écriture et de lecture.

quand le support de lecture et d'écriture devient numérique, cad : calculable, manipulable, duplicable, c'est également notre facon de penser qui se transforme.

Bruno Bachimont n'hésite pas à parler d'un changement de raison.



===d===

## 2.6 « disruption »

- Milad Doueihi, _La grande conversion numérique_, Seuil 2011
- Bernard Stiegler, _Dans la disruption&nbsp;: Comment ne pas devenir fou&nbsp;?_, Les Liens qui Libèrent 2016

===n===
Un grand bouleversement : Douhei parle de "grande conversion", qui advient avec Internet et le Web

Stiegler parle de _disruption_ : rupture majeure.

Et en tant que fait culturel, il transforme profondément **la façon dont on communique**, la facon dont on échange de l'information, la façon dont cette information est diffusée, c'est-à-dire aussi **les structures d'autorité** qui sous-tendaient cette diffusion et cette communication.

Crise institutionnelle pour l'édition, c'est-à-dire de ces intermédiaires. En effet, puisque leur fonction première était de donner forme au contenu, de produire leur support de circulation, et que ce support devient numérique tout change, y compris les aspects économiques, juridiques, sociaux, etc.

De nouveaux acteurs apparaissent dans l'environnement numérique

La fonction éditoriale change



===d===

## 3. Édition numérique

- les enjeux
- la conversion numérique des métiers du livre
- des bouleversements

===n===
Avant tout : de quoi parle-t-on ?
L'expression "édition numérique" peut être comprise dans différents sens, l'adjectif "numérique" ne voulant pas dire grand chose.
Attention à ne pas confondre avec _numérisation_ :

===d===

### 3.1 Enjeux

- techniques : de nouvelles façons de faire<!-- .element: class="fragment" -->
- théoriques : que fait le numérique à l'édition ?<!-- .element: class="fragment" -->
- sociaux : appropriation ou domination des processus<!-- .element: class="fragment" -->
- politiques : production et transmission des savoirs<!-- .element: class="fragment" -->

Comment s'orienter : avoir une approche critique face aux contenus, face aux outils et face aux plateformes.<!-- .element: class="fragment" -->

===n===

===d===

### 3.2. La conversion numérique des métiers du livre

- les bibliothèques : catalogues, bases de données<!-- .element: class="fragment" -->
- les éditeurs ensuite : traitements de texte, outils de publication assistée par ordinateur<!-- .element: class="fragment" -->
- la diffusion/distribution également : les métadonnées<!-- .element: class="fragment" -->
- les auteurs : changement subit ou provoqué<!-- .element: class="fragment" -->

===d===

<!-- .slide: data-background="images/cards.gif" data-background-size="contain" -->

===d===

<!-- .slide: data-background="images/macintosh.png" data-background-size="contain" -->

===d===

### 3.3. L'éditorialisation

> L’éditorialisation est l’ensemble des dynamiques qui constituent l’espace numérique et qui permettent à partir de cette constitution l’émergence du sens. Ces dynamiques sont le résultat de forces et d’actions différentes qui déterminent après-coup l’apparition et l’identification d’objets particuliers (personnes, communautés, algorithmes, plateformes...).  
Marcello Vitali-Rosati
<!-- .element: class="fragment" -->

===n===
Dans le numérique l'acte d'écriture est déjà un acte d'édition.

Peut-on écrire sans comprendre les enjeux de l'édition ? cad les enjeux des supports ?

Peut-on éditer sans le numérique aujourd'hui ? (Non)

===d===

## 4. Livre numérique

1. le livre déjà numérique<!-- .element: class="fragment" -->
2. l'arrivée du livre numérique en 3 points<!-- .element: class="fragment" -->
3. la difficile tâche de définition<!-- .element: class="fragment" -->

===d===

### 4.1. Le livre déjà numérique
**Rappel**, le livre et le numérique se rencontrent depuis les années 1980 :

- arrivée de l'informatique dès les premiers ordinateurs<!-- .element: class="fragment" -->
- conversion numérique des métiers et des outils<!-- .element: class="fragment" -->
- nécessité d'intégrer le flux imposé par Internet (infrastructure de diffusion, vente en ligne, communautés en ligne)<!-- .element: class="fragment" -->

===n===
Diffusion : le livre se _dématérialise_ pour répondre à des impératifs économiques.

Tout était en place pour permettre la conversion du livre lui-même.
Sauf peut-être les premiers concernés : les lecteurs.

===d===

### 4.2. L'arrivée du livre numérique en 3 points

1. dispositif de lecture : la liseuse à encre électronique<!-- .element: class="fragment" -->
2. format interopérable : le format standard EPUB<!-- .element: class="fragment" -->
3. offre commerciale : catalogue disponible<!-- .element: class="fragment" -->

===n===
Jusqu'ici les dispositifs de lecture expérimentaux ne permettaient pas un confort de lecture, le prix des premières liseuses étaient trop élevés (début des années 2000), Sony va commercialiser un modèle bon marché, suivi de près par Amazon.

Format ouvert du livre numérique permettant une interopérabilité, standard adopté par l'IDPF en 2007 et désormais maintenu par le W3C, l'EPUB remplace, dans le cas du livre numérique, les formats HTML, PDF ou txt.

Offre très limitée jusqu'au début des années 2000 : domaine public, éditeurs indépendants, formats contraints. Grâce à l'adoption d'un format : possibilité de produire des fichiers. Des éditeurs indépendants vont montrer la voie.

===d===

<!-- .slide: data-background="images/rocketbook.jpg" data-background-size="contain" -->

===n===
L'histoire du livre numérique est aussi une histoire de support !
La première tentative était quelque peu ratée : appareil trop lourd, écran lumineux, batterie courte, etc.

===d===

<!-- .slide: data-background="images/sony-datadiscman.jpg" data-background-size="contain" -->

===n===
Dès les premiers dispositifs de lecture numérique une dimension d'interaction avec le texte est présente : un clavier pour écrire _en même temps_ que lire.

===d===

<!-- .slide: data-background="images/sony-librie.jpg" data-background-size="contain" -->

===d===

### 4.3. La difficile tâche de définition
Ouvrage édité et diffusé sous forme numérique, destiné à être lu sur un écran. Note :

- Le livre numérique peut être un ouvrage composé directement sous forme numérique ou numérisé à partir d'imprimés ou de manuscrits.
- Le livre numérique peut être lu à l'aide de supports électroniques très divers.
- On trouve aussi le terme « livre électronique », qui n'est pas recommandé en ce sens.

_(Journal Officiel n°0081 du 4 avril 2012, page 6130, texte n° 118)_
