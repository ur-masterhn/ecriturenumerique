# Écritures numériques et éditorialisation

## Normes et standards de l'écriture numérique

séance 03/10 - 21 février 2020

---
<small>Master Humanités numériques de l'Université de Rouen</small>

===d===

## Plan de la séance

1. Normes et standards de l'écriture numérique
2. Tutoriel Markdown
3. TD Markdown

===d===

## 1. Normes et standards de l'écriture numérique

1. HTML vs XML<!-- .element: class="fragment" -->
2. nécessité de comprendre et maîtriser HTML<!-- .element: class="fragment" -->
3. vers une simplification du balisage<!-- .element: class="fragment" -->
4. introduction à Markdown<!-- .element: class="fragment" -->

===d===

## 1.1. HTML vs XML

- clôt vs extensible<!-- .element: class="fragment" -->
- standard vs standardisable<!-- .element: class="fragment" -->
- commun vs personnalisable<!-- .element: class="fragment" -->
- autonome vs lié à un schéma<!-- .element: class="fragment" -->
- destiné à l'affichage vs destiné à baliser<!-- .element: class="fragment" -->

===n===
Comme son nom l'indique (eXtensible Markup Language), XML est **extensible**.
HTML, et surtout dans sa version 5, n'est pas extensible.

Nous ne nous intéresserons pas à XML dans ses détails, ou uniquement en tant que format d'export.
Question : est-ce que des cours de XML TEI sont prodigués ?

===d===

## 1.2. Nécessité de comprendre HTML

- prendre la mesure de ce langage<!-- .element: class="fragment" -->
- comprendre le principe d'HTML(5)<!-- .element: class="fragment" -->
- appréhender la puissance d'un format standard et interopérable<!-- .element: class="fragment" -->

===d===

## 1.3. Vers une simplification du balisage
Écrire en HTML est possible mais inconfortable.

Comment contourner cette contrainte ?<!-- .element: class="fragment" -->


===n===
Contrairement au fait d'écrire en XML, qui est presque impossible.

===d===

## 1.4. Introduction à Markdown

- origine : création en 2004 par John Gruber et Aaron Swartz<!-- .element: class="fragment" -->
- objectif : écrire de l'HTML sans écrire de l'HTML<!-- .element: class="fragment" -->
- principe : balisage minimaliste<!-- .element: class="fragment" -->
- contrainte : balisage minimaliste<!-- .element: class="fragment" -->
- standardisation : plusieurs implémentations<!-- .element: class="fragment" -->

===n===
Markdown fait suite à d'autres langages de balisage léger.

Difficultés de se mettre d'accord sur une implémentation.

===d===

## 1.4. Introduction à Markdown
Markdown est un langage de balisage léger.

(Markdown est aussi un programme pour convertir un fichier au format Markdown en un autre format (HTML).)<!-- .element: class="fragment" -->

===d===

## 1.4. Introduction à Markdown
Distinctions nécessaires :

- le langage de balisage léger (Markdown)<!-- .element: class="fragment" -->
- le fichier au format Markdown<!-- .element: class="fragment" -->
- l'éditeur de texte (Atom, Codimd)<!-- .element: class="fragment" -->
- le programme de conversion (Pandoc)<!-- .element: class="fragment" -->

Le fichier `memoire.md` est un texte balisé en Markdown qui est converti au format HTML dans le fichier `memoire.html` avec le programme Pandoc.
<!-- .element: class="fragment" -->

===d===

## 2. Tutoriel Markdown
[https://artorig.github.io/tutomd/tutorial/](https://artorig.github.io/tutomd/tutorial/)
