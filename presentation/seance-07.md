# Écritures numériques et éditorialisation

## Les formats

séance 07/10 - 3 avril 2020

---
<small>Master Humanités numériques de l'Université de Rouen</small>

===d===

## Plan de la séance

1. informatique et formats
2. formats propriétaires et libres, plateformes
3. langages de balisage

===d===

## 1. Informatique et formats

- Une convention
- Des contraintes
- Une donnée invisibilisée

===d===

### 1.1. Une convention
Un format est une mise en forme des données basée sur des conventions de lecture.

Un format est une sorte de gabarit servant à représenter du son, du texte, de l'image, de la vidéo, ou une combinaison de ces éléments.

===d===

### 1.2. Des contraintes
Un format est fait pour être lu par un logiciel ou un programme.

Le format respecte des contraintes imposées par ce logiciel ou ce programme, ou à l'inverse un logiciel ou un programme impose des contraintes au format.

===d===

### 1.3. Une donnée invisibilisée

- le format est souvent indiqué par une extension de fichier : .pdf, .doc, .html<!-- .element: class="fragment" -->
- les systèmes d'exploitation peuvent interpréter ces extensions pour déterminer de quel format il s'agit, et quel logiciel associer à ce format<!-- .element: class="fragment" -->
- les systèmes d'exploitation (par exemple Windows) ont tendance à cacher ces extensions (un conseil : modifier les paramètres d'affichage pour ne pas les cacher)<!-- .element: class="fragment" -->
- <!-- .element: class="fragment" -->


===d===

## 2. Formats propriétaires et libres, plateformes

- formats propriétaires et libres
- interopérabilité
- plateformes

===d===

### 2.1. Formats propriétaires et libres

- formats propriétaires :<!-- .element: class="fragment" -->
  - fermés : leurs spécifications ne sont pas publiées, leur utilisation est brevetée et contrainte à leur application d'origine (par exemple .indd, InDesign Document)<!-- .element: class="fragment" -->
  - ouverts : leurs spécifications sont publiées, mais liés à des autorisations d'utilisation<!-- .element: class="fragment" -->
- formats libres : ils n'appartiennent pas à des entités privées et ne sont pas placés sous brevet. Tous le monde peut les lire !<!-- .element: class="fragment" -->

(Attention à ne pas confondre le type de format d'un fichier et les droits d'utilisation des contenus de ce fichier.)<!-- .element: class="fragment" -->

===d===

### 2.2. Interopérabilité
> Capacité que possède un produit ou un système [...] à fonctionner avec d’autres produits ou systèmes existants ou futurs et ce sans restriction d’accès ou de mise en œuvre.  
Source : [Wiktionnaire](https://fr.wiktionary.org/wiki/interop%C3%A9rabilit%C3%A9)

Article à lire : [Stéphane Bortzmeyer, C’est quoi, l’interopérabilité, et pourquoi est-ce beau et bien ?](https://framablog.org/2019/06/12/cest-quoi-linteroperabilite-et-pourquoi-est-ce-beau-et-bien/)

===d===

### 2.2. Interopérabilité
Sans interopérabilité nous serions obligés d'utiliser un logiciel par format, alors qu'il est possible d'utiliser plusieurs logiciels différents pour un même format ouvert ou libre.

===d===

### 2.3. Plateformes
En plus de l'invisibilisation des formats par les systèmes d'exploitation, les formats disparaissent avec la prédominance des plateformes.

Google Docs est un exemple emblématique&nbsp;: le format disparaît au profit de fonctionnalités d'édition.  
Dans un CMS (comme WordPress) nous n'avons pas conscience d'écrire dans un format particulier alors que nous produisons du HTML.

===d===

## 3. Langages de balisage

- langage de balisage : définition
- XML
- Markdown

===d===

### 3.1. Langage de balisage : définition
Un langage de balisage utilise des balises pour structurer un document.

Cette structure est compréhensible par un programme informatique, ce qui permet un traitement automatisé du contenu.

(Source : [Wikipédia](https://fr.wikipedia.org/wiki/Langage_de_balisage))

===d===

### 3.2. XML
Rapide focus sur XML :

- format extensible : il est toujours accompagné d'un schéma (nom et fonctionnement des balises)<!-- .element: class="fragment" -->
- format puissant : pour décrire de façon très poussée<!-- .element: class="fragment" -->
- format pivot : XML peut être la base pour développer des applications<!-- .element: class="fragment" -->

===d===

### 3.3. Markdown
Retour sur quelques notions importantes :

- c'est un langage de balisage _léger_

<!-- .element: class="fragment" -->

- Markdown est très utile pour _écrire_ mais il n'est pas publié en tant que tel

<!-- .element: class="fragment" -->

- Markdown ne se suffit pas à lui-même : il a vocation à être transformé en un autre format (HTML, XML, PDF, etc.), ce qui est possible parce qu'il est ouvert et libre

<!-- .element: class="fragment" -->

===d===

## 4. Implications politiques des formats

- enjeux d'interopérabilité : un format ouvert et libre permet d'envisager de plus grandes interactions (entre les formats et les logiciels, et entre les logiciels)<!-- .element: class="fragment" -->
- enjeux de pérennité : un format ouvert et libre, et donc documentable, pourra être lisible plus longtemps<!-- .element: class="fragment" -->
- enjeux d'évolutivité : un format ouvert et libre peut être augmenté et modifié<!-- .element: class="fragment" -->
