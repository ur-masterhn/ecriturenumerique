# Écritures numériques et éditorialisation

## Introduction

séance 01/10 - 7 février 2020

---
<small>Master Humanités numériques de l'Université de Rouen</small>


===d===

## Avant le tour de table

1. Tapez votre nom dans le pad :  
   [https://frama.link/mhn_introduction](https://frama.link/mhn_introduction)

1. Visitez la page et choisissez un livre !  
   [https://frama.link/10-livres](https://frama.link/10-livres)
===d===

## Antoine Fauchié & Nicolas Sauret

===d===

## À vous !

1. votre nom
2. pourquoi avoir choisi ce livre ?
3. d'où est-ce que vous venez ?
4. quelles sont vos attentes pour ce cours ?
5. et après ?

===n===
- trouver une contrainte (page web à choisir, livre numérique, page Wikipédia, etc.), point de départ
- d’où viennent les étudiants ?
- quelles attentes liées au cours ?
- quel futur envisagé ?

===d===

## Objectifs du cours

1. comprendre les enjeux de l'édition à l'époque du numérique
2. développer une analyse théorique des environnements techniques, des structures et des formats et des pratiques de lecture/écriture/édition
3. acquérir une approche critique des contenus en ligne et de leur environnement


===n===

**Objectifs**

1. enjeux :
    - transformation culturelle (conversion)
    - transformation des modes de savoir (on lit/écrit plus de la même facon)
    - transformation sociale (on n'échange plus de la meme manière)
2. analyse théorique :
    - ce qui structure _le numérique_ : le réseau, les serveurs, les plateformes, les systèmes de gestion de continu, les algorithmes des moteurs de recherche
    - ce qui permet la circulation du savoir : l'hypertexte, le multimédia, les métadonnées
    - la production de l'autorité
3. approche critique :
- comparer les formes éditoriales traditionnelles avec les récentes expériences d’édition des contenus en ligne
- comprendre de quelle manière les contenus sont structurés, publiés et diffusés sur Internet
- comprendre quelles sont les nouvelles possibilités offertes par ce _média_.
- quelles implication du point de vue créatif, artistique mais aussi social, politique, économique et légal (le rôle de l’auteur, les droits, le prix du livre, etc.).

===d===
## Objectifs des TDs

1. obtenir un aperçu des principaux formats du web
2. appréhender une chaîne éditoriale alternative
3. mettre en œuvre les bonnes pratiques en matière d'édition numérique

===d===

## Plan du cours

- 07/02 [séance 01] Introduction
- 14/02 [séance 02] Internet et web | _HTML_
- 21/02 [séance 03] _Markdown_
- 06/03 [séance 04] Édition numérique
- 13/03 [séance 05] Édition scientifique | _pandoc_
- 20/03 [séance 06] Édition enrichie | _indexation_
- 27/03 [séance 07] Formats | _conversion_
- 03/04 [séance 08] Humanités numériques | _Stylo_
- 10/04 [séance 09] Nouvelles écritures | _Stylo++_
- 17/04 [séance 10] Classe inversée

===n===
TDs en italique.

===d===
## Évaluation
Détails à venir dans les prochaines séances, 3 points importants :

- une réalisation
- une présentation de la chaîne utilisée
- un travail d'analyse

===n===
explication sur les travaux/rendus

===d===
## Ressources et environnement

- visioconférence : Jitsi ([https://meet.jit.si](https://meet.jit.si))
- éditeur de texte : Atom ([https://atom.io/](https://atom.io/))
- terminal : déjà installé sous Linux et Mac
- convertisseur : pandoc ([https://pandoc.org](https://pandoc.org)) et pandoc-citeproc ([https://github.com/jgm/pandoc-citeproc](https://github.com/jgm/pandoc-citeproc))

===n===
présentation de l’environnement de travail : notes, pandoc, terminal

===d===
## Prise de note collaborative

[https://frama.link/mhn_introduction](https://frama.link/mhn_introduction)

===d===
## Comment nous contacter ?
Nicolas Sauret : [nnicolaoss@gmail.com](mailto:nnicolaoss@gmail.com)

Antoine Fauchié : [antoine@quaternum.net](mailto:antoine@quaternum.net)

===d===
## Pour la prochaine séance

- installez Atom ([https://atom.io](https://atom.io))
- créez-vous un compte Framagit ([https://framagit.org](https://framagit.org))
- prenez connaissance de la bibliographie : [https://www.zotero.org/groups/2365743/critures_numrique_-_master_humanits_numriques/library](https://www.zotero.org/groups/2365743/critures_numrique_-_master_humanits_numriques/library)
