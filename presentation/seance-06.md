# Écritures numériques et éditorialisation

## Le livre numérique

séance 06/10 - 27 mars 2020

---
<small>Master Humanités numériques de l'Université de Rouen</small>

===d===

## Plan de la séance

1. Contexte d'émergence du livre numérique
2. Définition technique et juridique
3. Supports et formats

===d===

## 1. Contexte d'émergence du livre numérique

- le livre : un objet hautement technologique
- les rencontres du livre et du numérique
- le premier livre numérique

===d===

### 1.1. Le livre : un objet hautement technologique
Avant toute chose, voir la vidéo qui suit ↓

===d===

<iframe width="1120" height="630" src="https://www.youtube.com/embed/cgcSxQiS0RM" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

===d===

### 1.1. Le livre : un objet hautement technologique
Saut technique important : le rouleau était très contraignant, il fallait deux mains et un menton pour pouvoir lire.

Les premiers codex seront certes assez encombrants, mais ils ouvrent de nouvelles perspectives : plus de contenus, une meilleure lisibilité, une structuration et un stockage facilité.

===d===

### 1.2. Les rencontres du livre et du numérique

1. les communautés en ligne permises par Internet : de Zazieweb à Babelio<!-- .element: class="fragment" -->
2. le commerce en ligne : Amazon ouvre en 1994<!-- .element: class="fragment" -->
3. la littérature numérique : le web comme espace de création<!-- .element: class="fragment" -->

===d===

### 1.2. Les rencontres du livre et du numérique
En plus de la conversion numérique des métiers du livre ([voir séance 4](https://ur-masterhn.frama.io/ecriturenumerique/presentation/#/3/16)), ces 3 rencontres précèdent l'arrivée du livre numérique, et structurent même cette arrivée sur plusieurs niveaux :

- technique : logiciels et pratiques de création<!-- .element: class="fragment" -->
- réseau : les acteurs sont connectés entre eux<!-- .element: class="fragment" -->
- usages : les lecteurs ont déjà des pratiques de lecture numérique<!-- .element: class="fragment" -->

===d===

### 1.3. Le premier livre numérique

- le 4 juillet 1971 Michael Hart crée un fichier pour partager _The United States Declaration of Independence_ ([voir en ligne](http://www.gutenberg.org/cache/epub/1/pg1.txt))

<!-- .element: class="fragment" -->

- il s'agit d'un fichier texte sans mise en forme ni structuration

<!-- .element: class="fragment" -->

- ce _premier_ livre numérique sera le point de départ du [projet Gutenberg](https://www.gutenberg.org/), la première bibliothèque de livres numériques

<!-- .element: class="fragment" -->

===d===

## 2. Définition technique et économique

- retour sur la définition légale
- comment reconnaître un livre numérique ?
- un débat lié aux enjeux économiques

===d===

### 2.1. Retour sur la définition légale
Ouvrage édité et diffusé sous forme numérique, destiné à être lu sur un écran. Note :

- Le livre numérique peut être un ouvrage composé directement sous forme numérique ou numérisé à partir d'imprimés ou de manuscrits.
- Le livre numérique peut être lu à l'aide de supports électroniques très divers.
- On trouve aussi le terme « livre électronique », qui n'est pas recommandé en ce sens.

_(Journal Officiel n°0081 du 4 avril 2012, page 6130, texte n° 118)_

===d===

### 2.1. Retour sur la définition légale
Points importants liés à la définition _légale_ du livre numérique :

- "édité et diffusé sous forme numérique" : c'est autant l'état du livre que la façon dont il circule

<!-- .element: class="fragment" -->

- "sur un écran" : définition large qui n'est pas restreinte à un dispositif de lecture (une liseuse ou une tablette par exemple)

<!-- .element: class="fragment" -->

- "composé directement sous forme numérique ou numérisé à partir d'imprimés ou de manuscrits" : le livre numérique n'est pas forcément _nativement_ numérique

<!-- .element: class="fragment" -->

===d===

### 2.2. Comment reconnaître un livre numérique ?

- la notion de "livre numérique" concerne avant tout le contenu, et donc plus le fichier que le support

<!-- .element: class="fragment" -->

- un livre numérique peut être lu avec un dispositif de lecture numérique

<!-- .element: class="fragment" -->

- _normalement_ un livre numérique est un format _liquide_ (le format EPUB, voir partie 3 de cette séance)

<!-- .element: class="fragment" -->

===d===

### 2.2. Comment reconnaître un livre numérique ?
Quelques questions ouvertes :

- un fichier PDF est-il un livre numérique ?<!-- .element: class="fragment" -->
- une application est-elle un livre numérique ?<!-- .element: class="fragment" -->
- un site web est-il un livre numérique ?<!-- .element: class="fragment" -->

Nous fixerons ensemble quelques critères pour essayer de répondre à ces questions.<!-- .element: class="fragment" -->

===d===

### 2.3. Un débat lié aux enjeux économiques
Le livre numérique est-il un bien ou un service ?

- si le livre est un bien : TVA identique à celle du livre imprimé (5,5 % en France)<!-- .element: class="fragment" -->
- si le livre numérique est un service : TVA de 20 %<!-- .element: class="fragment" -->

===d===

### 2.3. Un débat lié aux enjeux économiques
Ce problème s'est résolu récemment, la plupart des acteurs s'accordant sur : livre numérique = bien

Mais dans les faits le livre numérique s’apparente aujourd'hui à un service : pas d'accès au fichier lui-même, données liées à un compte et une plateforme en ligne, revendeur qui a le dernier mot ([court article à lire](https://www.lemonde.fr/technologies/article/2009/07/22/amazon-jette-1984-dans-le-trou-de-memoire_1221324_651865.html)).

<!-- .element: class="fragment" -->

===d===

## 3. Supports et formats

- définition d'un dispositif de lecture
- les supports de lecture numérique
- formats

===d===

### 3.1. Définition d'un dispositif de lecture
Il s'agit avant tout d'une définition d'un rapport _physique_ à un objet. Un dispositif de lecture doit :

- permettre une disposition de lecture<!-- .element: class="fragment" -->
- être relativement léger et maniable<!-- .element: class="fragment" -->
- proposer une ergonomie satisfaisante<!-- .element: class="fragment" -->
- limiter les contraintes (lire à un bureau est par exemple une contrainte)<!-- .element: class="fragment" -->

===d===

### 3.2. Les supports de lecture numérique
À partir de la définition d'un dispositif de lecture, voici quelques exemples de dispositifs de lecture numérique&nbsp;:

- une liseuse à encre électronique : léger, confort de l'écran<!-- .element: class="fragment" -->
- une tablette (écran à affichage LED) : léger, ergonomique, l'écran peut fatiguer<!-- .element: class="fragment" -->
- un téléphone intelligent (ou smartphone) : léger, ergonomique, écran trop petit ?<!-- .element: class="fragment" -->
- ordinateur portable : peu léger, peu ergonomique pour la lecture, est-ce un dispositif de lecture numérique ?<!-- .element: class="fragment" -->

===d===

### 3.2. Les supports de lecture numérique
Qu'est-ce que l'encre électronique ?

- un écran à encre électronique est composé de capsules rempli de poudre<!-- .element: class="fragment" -->
- un champ magnétique permet de changer l'état des capsule, et donc d'obtenir des pixels noir ou blanc (et même gris)<!-- .element: class="fragment" -->
- une source d'énergie est nécessaire uniquement pour le changement d'état<!-- .element: class="fragment" -->
- résultat : confort de lecture (pas de production lumineuse) et peu d'énergie nécessaire au fonctionnement<!-- .element: class="fragment" -->

Pour en savoir plus [voir l'article détaillé sur Wikipédia](https://fr.wikipedia.org/wiki/Papier_%C3%A9lectronique).

<!-- .element: class="fragment" -->

===d===

<!-- .slide: data-background="images/encre-electronique.png" data-background-size="contain" data-background-color="#ffffff" -->

===d===

### 3.3. Formats
Distinction préalable :

- support de lecture : matériel composé d'un écran, d'une batterie et de composants électroniques<!-- .element: class="fragment" -->
- interface de lecture : logiciel permettant de lire un fichier dans un format précis<!-- .element: class="fragment" -->
- fichier : il contient les données<!-- .element: class="fragment" -->
- format : façon dont est structuré/encodé un fichier<!-- .element: class="fragment" -->

===d===

### 3.3. Formats
Un format est une convention pour représenter des données : pour que des programmes/logiciels puissent lire des formats, il faut que les formats soient standardisés.

Un standard : une suite de règles à respecter pour qu'un format soit lisible (exemple : le format HTML est lisible de la même façon sur tous les navigateurs parce qu'il respecte des standards).

<!-- .element: class="fragment" -->

===d===

### 3.3. Formats
Le format pour le livre numérique : EPUB.

- techniquement : un site web encapsulé, donc fichiers HTML et CSS (et des métadonnées)<!-- .element: class="fragment" -->
- fonctionnalités : adaptation du texte à la taille de l'écran, réglages typographiques possibles comme les marges, les polices, etc. (contrairement au PDF)<!-- .element: class="fragment" -->
- économiquement : format qui peut être vendu<!-- .element: class="fragment" -->
- protection : DRM Adobe ou marquage (watermark) pour protéger le fichier<!-- .element: class="fragment" -->

===d===

### 3.3. Formats
Plus de renseignements sur l'EPUB et d'autres formats à la prochaine séance, en attendant une lecture pour la semaine prochaine :

[L’ebook redistribuable par Jiminy Panoz](https://jaypanoz.github.io/reflow/)
