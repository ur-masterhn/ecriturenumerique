# UR-MasterHN-ecriturenumerique

Présentation (slides) et ressources TD du cours **Écritures numériques et éditorialisation** du Master Humanités numériques de l'Université de Rouen.

Bienvenu dans le cours **Écritures numériques et éditorialisation**. Vous trouverez ici toutes les ressources pour suivre le cours et faire les travaux dirigés.

En préparation du cours, merci
1. [de vous créer un compte sur Framagit](https://framagit.org/users/sign_in?redirect_to_referer=yes) (et de nous envoyer votre username) et
2. [d'installer les outils nécessaires](./ressources/outils.md).

## Protocole pour le cours à distance
Ce cours étant donné à distance par Nicolas Sauret et Antoine Fauchié (localisés à Montréal), voici quelques indications sur le déroulement et le protocole proposé :

- cours magistral donné via l'affichage sur grand écran ;
- TD donné via cet écran avec deux autres canaux de communication possibles, et individuels :
  - pour discuter individuellement avec Antoine Fauchié : [https://meet.jit.si/ene-antoine](https://meet.jit.si/ene-antoine)
  - pour discuter individuellement avec Nicolas Sauret : [https://meet.jit.si/ene-nicolas](https://meet.jit.si/ene-nicolas)

Ces deux canaux de discussion différents nous permettront de vous accompagner pendant les travaux dirigés, et de lever facilement toute difficulté en partageant vos et nos écrans.

## Prise de note collaborative
Nous vous invitons à prendre des notes pendant le cours, de façon collaborative, sur le _pad_ suivant :

[https://frama.link/mhn_introduction](https://frama.link/mhn_introduction)

À chaque début de séance nous reprendrons les éléments indiqués dans le pad pour éventuellement préciser certains points.

## Annotation des lectures

Dans le cadre de la continuité pédagogique, nous vous proposons une lecture collective et à distance des textes, grâce à l'outil Hypothes.is. Cet outil est largement utilisé par les chercheurs et les enseignants, n'hésitez pas à l'employer au-delà de ce cours.

Pour bien annoter, visitez [la ressource dédiée (tuto et prise en main)](./ressources/annotation.md).

## Plan de cours

### Séance 1 - vendredi 7 février - 2h

- introduction [[présentation (navigation avec la flèche du bas)](https://ur-masterhn.frama.io/ecriturenumerique/presentation/#/)]

### Séance 2 - vendredi 14 février - 2h

- théorie: [[présentation (navigation avec la flèche du bas)](https://ur-masterhn.frama.io/ecriturenumerique/presentation/#/1)]
  - Internet et le web: principes de base
  - Les algorithmes
- travaux dirigés: [principes du html](travauxdiriges/seance2_html.md)

### Séance 3 - vendredi 21 février - 2h

- théorie [[présentation (navigation avec la flèche du bas)](https://ur-masterhn.frama.io/ecriturenumerique/presentation/#/2)]
- travaux dirigés: [principes du markdown](travauxdiriges/seance3_markdown.md)
- livrable: édition d'un article au format Markdown

### Séance 4 - vendredi 6 mars - 2h

- théorie [[présentation (navigation avec la flèche du bas)](https://ur-masterhn.frama.io/ecriturenumerique/presentation/#/3)]
    - qu'est-ce que l'édition, qu'est-ce que le numérique, qu'est-ce que l'écriture?
    - le livre numérique
    - modèle économique, juridique

### Séance 5 - vendredi 13 mars - 2h

- théorie: édition scientifique [[présentation (navigation avec la flèche du bas)](https://ur-masterhn.frama.io/ecriturenumerique/presentation/#/4)]
- travaux dirigés: [initiation à Stylo](travauxdiriges/seance5_stylo.md)

### Séance 6 - vendredi 20 mars - 2h

- théorie: Livre numérique, le cas de l'édition enrichie «\ Exigeons de nouvelles bibliothèques\ »
- lectures :
  - **Un chapitre du l'ouvrage pour bien en faire l'expérience :** R. David Lankes, « Un plan d’action : exigeons de meilleures bibliothèques », Exigeons de meilleures bibliothèques (édition augmentée), Les Ateliers de [sens public], Montréal, isbn:978-2-924925-09-6, http://ateliers.sens-public.org/exigeons-de-meilleures-bibliotheques/chapitre8.html. version 1, 28/10/2019 (CC BY-SA 4.0)
  - **Un chapitre [lire les pages 10 à 19 du PDF] plus pédagogique sur la fonction éditoriale à l'heure du livre numérique :** Benoît Epron et Marcello Vitali-Rosati, « Les éditeurs face au numérique », _L'édition à l'ère numérique_, La Découverte, Paris, isbn:978-2-7071-9935-5, https://papyrus.bib.umontreal.ca/xmlui/bitstream/handle/1866/20642/Epron_Vitali-Rosati_EditionEreNumerique.pdf?sequence=1&isAllowed=y#%5B%7B%22num%22%3A28%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C90%2C320.9%2C0%5D (version preprint disponible en ligne)

### Séance 7 - vendredi 27 mars - 2h

- théorie: les formats et leurs implications théoriques et politiques (différences entre Markdown/AsciiDoc/LaTeX/HTML/XML/TEI)
- Lectures:
  1. Boulétreau Viviane, Habert Benoît (2014). “Les formats”, in E. Sinatra Michael, Vitali-Rosati Marcello (édité par), Pratiques de l’édition numérique, collection « Parcours Numériques », Les Presses de l’Université de Montréal, Montréal, p. 145-159, ISBN: 978-2-7606-3202-8 (http://www.parcoursnumeriques-pum.ca/Les-formats)
  2. Trois textes d'Olivier Ertzscheid liés à la question des plateformes :
      1. Ertzscheid, Olivier. « Chronographie algorithmique : pour une définition des publications industrielles. » affordance.info, 16 mars 2016. http://www.affordance.info/mon_weblog/2016/03/chronographie-algorithmique.html.
      2. ———. « Editorialisation algorithmique. » affordance.info, 13 mai 2016. http://affordance.typepad.com/mon_weblog/2016/05/editorialisation-algorithmique.html.
      3. ———. « Un algorithme est un éditorialiste comme les autres. » affordance.info, 15 novembre 2016. http://www.affordance.info/mon_weblog/2016/11/un-algorithme-est-un-editorialiste-comme-les-autres.html.

À partir de maintenant, nous vous invitons à une lecture collective des textes (y compris ceux de la semaine précédente pour ceux qui le souhaitent). Pour en savoir plus, voir [ci-dessus l'annotation des lectures](#annotation-des-lectures).

### Séance 8 - vendredi 3 avril - 2h

- théorie: DH, histoire et philosophie d'un champ
- travaux dirigés: gestion biblio (vidéo) + tutoriel export.

### Séance 9 - vendredi 10 avril - 2h

- théorie: nouvelles écritures, écritures dispositives, conversation
- travaux dirigés: [Stylo avancé](travauxdiriges/seance9.md)

### Séance 10 - vendredi 17 avril - 3h

- classe inversée:
    - numérisation
    - édition native
    - édition collective
