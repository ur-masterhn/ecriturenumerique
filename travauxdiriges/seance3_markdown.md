# TD Séance 3 - Le format Markdown

Après [une brève présentation théorique](https://ur-masterhn.frama.io/ecriturenumerique/presentation/#/2) du format Markdown, passons à la pratique.

## 1. Tutoriel de prise en main
Pour faire vos premiers pas, voici un tutoriel à faire en 20 minutes : [artorig.github.io/tutomd/](https://artorig.github.io/tutomd/)

_À noter : CommonMark est une implémentation du format Markdown. On y retrouve pratiquement la même syntaxe._

## 2. Exercice: édition d'un article

Vous allez maintenant éditer individuellement un article dans le format Markdown. Pour écrire au format Markdown, on peut utiliser différents éditeurs :
- _en local_ avec des éditeurs comme Atom.io, Textedit, Notepad, etc. (mais pas Word!!),
- _en ligne_ comme Codi par exemple.

L'exercice consiste à récupérer le contenu d'un article en ligne, et à en faire une **édition numérique au format Markdown**.

Article source : «Stratégie anti-pauvreté» de Niels Planel [sur Sens Public](http://sens-public.org/article1359.html).


### 1. Créer un fichier `article.md`

- dans votre éditeur de texte, créez un nouveau fichier et sauvegardez le dans le répertoire `ecrinum-prenom` [créé la semaine dernière](./seance2_html.md). Appelez ce nouveau fichier  `article.md`
- son extension est `.md`, votre éditeur de texte va donc reconnaitre le format Markdown de son contenu.

### 2. reporter (copier-coller) le texte de l'article dans votre éditeur de texte

### 3. éditer le texte en Markdown et traiter notamment :
- les titres
- les paragraphes
- les citations
- les hyperliens
- les italiques
- les espaces insécables: en ASCII `&nbsp;` (pour _Non-Breaking SPace_) selon [les règles typographiques en vigueur](http://monsu.desiderio.free.fr/atelier/espace.html) (voir aussi sur  [la Banque de dépannage linguistique](http://bdl.oqlf.gouv.qc.ca/bdl/gabarit_bdl.asp?id=2039)). Par exemple :

  ```markdown
  Bonjour&nbsp;!

  «&nbsp;Comment ca va&nbsp;?&nbsp;»
  ```

### 4. à titre pédagogique, ajouter des éléments supplémentaires :
- 2 niveaux de titre: `#` niveau 1 et `##` niveau 2  
- une image: `![Légende de l'image](http://lien.ca/vers/image.jpg)`
- une ou plusieurs note.s de bas de page: `[^note]` pour l'appel de note dans le texte et `[^note]: Ma note.` pour le texte de la note en fin de document. Par exemple

  ```markdown
  Cet homme[^manote] est assez grand.

  [^manote]: Il mesure 1 mètre 80.
  ```

### Visualisez votre édition !

Il existe des solutions simples pour visualiser du Markdown en HTML. Nous allons utiliser l'éditeur Markdown en ligne Codi que vous connaissez déjà, puisqu'on l'utilise [pour la prise de note](https://frama.link/mhn_introduction).

- cliquez sur le lien [demo.codimd.org](https://demo.codimd.org/), puis cliquez sur le bouton [+Nouvelle note] (ou [+New note] en anglais)
- votre éditeur est prêt à l'emploi. Copiez le contenu du fichier `article.md` et collez le dans l'éditeur Codi.
- En haut à gauche de la page, trois boutons vous permettent de passer d'un mode "Édition" (pour écrire) aux modes "Visualisation" (pour lire) ou "Les deux" (pour lire et écrire en même temps)
- partagez avec nous votre travail en copiant l'url de votre note Codi sur la prise de note : https://frama.link/mhn_introduction
