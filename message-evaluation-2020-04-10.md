# Message du 10 avril concernant l'évaluation

Objet : [Master HN] Évaluation : description du travail à réaliser

Bonjour,

Ce message concerne l'évaluation de ce cours, merci de prendre le temps de le lire ainsi que le document sur la plateforme du cours.

Tout est détaillé dans ce document : https://framagit.org/ur-masterhn/ecriturenumerique/-/blob/master/ressources/evaluation.md

Voici un résumé succinct :

- il s'agit d'une analyse à réaliser individuellement ;
- vous avez jusqu'au vendredi 17 avril minuit pour nous rendre ce travail par mail ;
- l'analyse est à rédiger avec Stylo, le rendu est donc la preview (un lien à nous indiquer par mail) ;
- vous pouvez faire toutes les recherches que vous souhaitez, vous devez surtout vous appuyer sur le cours et les lectures indiquées pendant les séances.

Nous restons à votre disposition Nicolas et moi si vous avez des questions (merci de ne pas oublier de nous adresser vos messages à nos deux adresses).

Bonne lecture et bon travail.
